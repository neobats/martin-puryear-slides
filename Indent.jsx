import React from 'react'

const Indent = ({ text }) => <span style={{ marginLeft: '40px' }}>{text}</span>

export default Indent
