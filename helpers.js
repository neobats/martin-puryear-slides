export const checkSrc = path =>
  path.includes('.jpg') || path.includes('.png') ? path : `/assets/${path}.jpg`

const capitalize = word => word[0].toLocaleUpperCase().concat(word.slice(1))
export const stripName = src =>
  src
    .replace(/\/static\/(.*)-(.*)/, '$1')
    .split('_')
    .map(capitalize)
    .join(' ')
