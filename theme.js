import { themes } from 'mdx-deck'
import Blockquote from './blockquote'

// export default {
//   space: [0, 4, 8, 16, 32, 64, 128, 256, 512],
//   fonts: {
//     body: '"Alegreya", Georgia, Times, serif',
//     // body: '"Asap", Helvetica, Arial, sans-serif',
//   },
//   googleFont:
//     "https://fonts.googleapis.com/css2?family=Alegreya:ital,wght@0,400;0,700;1,400;1,700&display=swap",
//   // "https://fonts.googleapis.com/css2?family=Asap:ital,wght@0,400;0,700;1,400;1,700&display=swap",
//   fontWeights: {
//     body: 400,
//     heading: 700,
//     bold: 700,
//   },
//   lineHeights: {
//     body: 1.5,
//     heading: 1.125,
//   },
//   colors: {
//     text: "#000",
//     background: "#BA925F",
//     primary: "#6B00CC",
//     secondary: "#A07CC5",
//     muted: "#f6f6f6",
//   },
// };

export default {
  ...themes.default,
  fonts: {
    body: "'Asap', Helvetica, Arial, sans-serif",
    heading: '"Alegreya", Georgia, Times, serif',
  },
  googleFont:
    'https://fonts.googleapis.com/css2?family=Alegreya:wght@400;700&family=Asap:ital@0;1&display=swap',
  colors: {
    text: '#000',
    background: '#BA925F',
  },
  lineHeights: {
    body: 2,
    heading: 1.125,
  },
  components: {
    blockquote: Blockquote,
  },
}
