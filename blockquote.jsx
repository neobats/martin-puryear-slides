import React from 'react'

const Blockquote = ({ children }) => (
  <blockquote
    style={{
      fontStyle: 'italic',
      paddingLeft: '1.4em',
      marginLeft: '1em',
      borderLeft: '.3em solid #6B00CC',
      borderRadius: '15px',
      width: 'min(60ch, 80%)',
    }}
  >
    {children}
  </blockquote>
)

export default Blockquote
