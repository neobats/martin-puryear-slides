import { Split } from 'mdx-deck'
import React from 'react'
import { stripName } from './helpers'
import Img from './img'

export default function ImgRight({ src, children }) {
  return (
    <Split>
      <div>
        <h1
          style={{
            fontFamily: '"Alegreya", Georgia, serif',
          }}
        >
          {stripName(src)}
        </h1>
        {children}
      </div>
      <Img src={src} />
    </Split>
  )
}
