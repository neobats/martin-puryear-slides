import React from 'react'
import { checkSrc, stripName } from './helpers'

const Img = props => {
  return (
    <img
      src={checkSrc(props.src)}
      style={{
        boxShadow: '0 0 50px 10px rgba(0,0,0, 0.6)',
        width: '90%',
      }}
      alt={stripName(props.src)}
      {...props}
    />
  )
}

export default Img
